<?php

namespace App\Http\Controllers;

use App\Http\Requests\SocialLoginRequest;
use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param SocialLoginRequest $request
     */
    public function __invoke(SocialLoginRequest $request)
    {
        // Get User from social provider, such as Google and Facebook
        try {
            $user = Socialite::driver($request->input('provider'))->userFromToken($request->input('token'));
        } catch (ClientException $clientException) {
            if ($clientException->getCode() == 401) {
                abort(401, "Something went wrong when retrieving the user from the provider. Did the token expire?");
            }
        }

        // Check if a user with this social media account already exists, otherwise create new
        $user = User::firstOrCreate([
            'social_provider' => $request->input('provider'),
            'social_provider_id' => $user->getId()
        ], [
            'email' => $user->getEmail(),
            'name' => $user->getName(),
            'avatar' => $user->getAvatar(),
            'email_verified_at' => now(),
        ]);

        // Generate a Sanctum token
        $token = null;

//        TODO: Check if token already set
//        if(!$user->tokens()->where('name', $request->device_name)->exists()) {
//            $token = $user->createToken($request->device_name)->plainTextToken;
//        }

        $token = $user->createToken($request->device_name)->plainTextToken;

        return response()->json([
            'user' => $user,
            'token' => $token != null ? $token : ''
        ], 200);
    }
}
