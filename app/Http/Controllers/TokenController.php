<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TokenController extends Controller
{

    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        return auth()->user()->tokens ?? [];
    }

    /**
     * @param Request $request
     * @param int $tokenId
     * @return array
     */
    public function delete(Request $request, int $tokenId) {
        auth()->user()->tokens()->findOrFail($tokenId)->delete();
        return auth()->user()->tokens ?? [];
    }
}
