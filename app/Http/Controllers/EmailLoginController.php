<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmailLoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class EmailLoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param EmailLoginRequest $request
     */
    public function __invoke(EmailLoginRequest $request)
    {
        // Check if a user with this social media account already exists, otherwise create new
        $user = User::firstWhere(
            'email', $request->input('email'),
        );

        if (!Hash::check($request->input('password'), $user->password)) {
            abort(422);
        }

        $token = $user->createToken($request->device_name)->plainTextToken;

        return response()->json([
            'user' => $user,
            'token' => $token != null ? $token : ''
        ], 200);
    }
}
