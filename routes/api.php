<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmailLoginController;
use App\Http\Controllers\SocialLoginController;
use App\Http\Controllers\TokenController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::post('/auth/login/social', SocialLoginController::class)->name('socialLogin');
Route::post('/auth/login/email', EmailLoginController::class)->name('emailLogin');

Route::middleware('auth:sanctum')->group(function () {

    Route::post('/auth/logout', [AuthController::class, 'logout'])->name('logout');

    // User
    Route::get('/user', [UserController::class, 'getAuthUser']);

    // Devices
    Route::get('/user/devices', [TokenController::class, 'index']);
    Route::delete('/user/device/{tokenId}', [TokenController::class, 'delete']);
});
